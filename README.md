# Libint for yucca

This is a helper repo for generating [libint](https://github.com/evaleev/libint2) configurations for
[yucca](https://gitlab.com/team-parker/yucca) and making them available for download.
The releases of this repo can be downloaded as tarballs, then compiled and used with yucca.

Available versions:
  - [v2.9.0 with generic tuning](https://gitlab.com/team-parker/libint-yucca/-/jobs/artifacts/master/raw/libint2-v2.9.0-g++-generic.tar.xz?job=build290)
  - [v2.8.2 with generic tuning](https://gitlab.com/team-parker/libint-yucca/-/jobs/artifacts/master/raw/libint2-v2.8.2-g++-generic.tar.xz?job=build282)
