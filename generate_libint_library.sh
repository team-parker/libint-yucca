#!/bin/bash

LIBINT_version=v2.9.0
COMPILER="g++"
ARCHTUNE="generic"
OPTFLAGS="-O2"
CPPFLAGS='-I/opt/local/include'
LDFLAGS='-L/opt/local/lib'
PARALLEL=""

archtune_flags() {
  archtune=$1
  if [ "$archtune" == "generic" ]; then
    echo "-mtune=generic"
  else
    echo "-march=${archtune} -mtune=${archtune}"
  fi
}

usage() {
  echo "This script requires all the prerequisites of libint2:"
  echo "  - automake"
  echo "  - autoconf"
  echo "  - libgmp with c++ support (apt: libgmp-dev)"
  echo "  - libboost (apt: libboost-dev)"
  echo "  - eigen3"
  echo
  echo "Setting up on ubuntu:"
  echo "  sudo apt install automake libgmp-dev libboost-dev"
  echo
  echo "Setting up on macOS with MacPorts:"
  echo "  sudo port install automake autoconf gmp boost eigen3"
  echo
  echo "Usage:"
  echo "  $0 [options]"
  echo
  echo "Options:"
  printf "  %-20s %-80s\n" "-c <compiler>"  "compiler used for generation (def: ${COMPILER})"
  printf "  %-20s %-80s\n" "-o <options>"   "optimization flags (def: ${OPTFLAGS})"
  printf "  %-20s %-80s\n" "-t <arch>"   "tune flags (def: ${ARCHTUNE})"
  printf "  %-20s %-80s\n" "-j <ncpus>"   "build in parallel (possibly ignored)  (def: system default)"
  printf "  %-20s %-80s\n" "-v <version>"   "libint version or git tag (def: ${LIBINT_version})"
  printf "  %-20s %-80s\n" "-i <includes>"  "include options for compilation (def: ${CPPFLAGS})"
  printf "  %-20s %-80s\n" "-l <libraries>" "library options for compilation (def: ${LDFLAGS})"
}

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
  usage
  exit
fi

while getopts ":b:c:o:t:v:i:l:" opt; do
  case ${opt} in
    c )
      COMPILER="$OPTARG"
      ;;
    o )
      OPTFLAGS="$OPTARG"
      ;;
    t )
      ARCHTUNE="$OPTARG"
      ;;
    v )
      LIBINT_version="$OPTARG"
      ;;
    i )
      CPPFLAGS="$OPTARG"
      ;;
    l )
      LDFLAGS="$OPTARG"
      ;;
    \? )
      echo "Invalid option: $OPTARG"
      ;;
    : )
      echo "Invalid option: $OPTARG required"
      ;;
  esac
done
shift $((OPTIND -1))

EXPORT_name=libint2-${LIBINT_version}-${COMPILER}-${ARCHTUNE}
GENERATOR_OPTS="${OPTFLAGS} $(archtune_flags ${ARCHTUNE})"
LIBINT_dir=libint2
BUILD_dir=build-${LIBINT_version}-${COMPILER}-${ARCHTUNE}

echo "Using libint2 options:"
echo "libint version: ${LIBINT_version}"
echo

echo "Using configure options:"
echo "Targeting compiler: ${COMPILER}"
echo "Targeting compiler optimization flags: ${GENERATOR_OPTS}"
echo "CPPFLAGS=${CPPFLAGS}"
echo "LDFLAGS=${LDFLAGS}"
echo

echo "Library will be produced into ${EXPORT_name}.tgz"

#set -euxo pipefail
set -euo pipefail

if [ ! -d $LIBINT_dir ]; then
  mkdir -p $LIBINT_dir
  echo "Cloning libint from github"
  git clone https://github.com/evaleev/libint.git $LIBINT_dir
fi

cd $LIBINT_dir
echo "Checking out $LIBINT_version"
git fetch
git checkout $LIBINT_version

# split version string into major/minor/patch
major_version=${LIBINT_version:1:1}
minor_version=${LIBINT_version:3:1}
patch_version=${LIBINT_version:5:1}

echo "Configuring libint"
./autogen.sh
if [ -d $BUILD_dir ]; then
  rm -rf $BUILD_dir
fi
mkdir $BUILD_dir
cd $BUILD_dir

# before 2.8.2, need to avoid a bug
if [ $major_version -lt 2 ] || [ $major_version -eq 2 ] && [ $minor_version -le 8 ] && [ $patch_version -le 1 ]; then
../configure --enable-eri=1 --enable-eri2=1 --enable-eri3=1 \
  --enable-1body=1 \
  --with-max-am=6 \
  --with-opt-am=3 \
  --enable-contracted-ints \
  --with-cartgauss-ordering=standard \
  --with-shell-set=standard \
  --with-libint-exportdir=$EXPORT_name \
  --with-cxxgen=$COMPILER \
  --with-cxxgen-optflags="$GENERATOR_OPTS" \
  --enable-generic-code --disable-unrolling \
  --with-boost=no \
  CXX="${COMPILER}" \
  CPPFLAGS="$CPPFLAGS" \
  CXXFLAGS="$CPPFLAGS" \
  LDFLAGS="$LDFLAGS"
else
../configure --enable-eri=1 --enable-eri2=1 --enable-eri3=1 \
  --enable-1body=1 \
  --with-max-am=4 \
  --with-opt-am=3 \
  --with-eri-max-am=4 \
  --with-eri2-max-am=6 \
  --with-eri3-max-am=6 \
  --enable-contracted-ints \
  --with-cartgauss-ordering=standard \
  --with-shell-set=standard \
  --with-libint-exportdir=$EXPORT_name \
  --with-cxxgen=$COMPILER \
  --with-cxxgen-optflags="$GENERATOR_OPTS" \
  --enable-generic-code --disable-unrolling \
  --with-boost=no \
  CXX="${COMPILER}" \
  CPPFLAGS="$CPPFLAGS" \
  CXXFLAGS="$CPPFLAGS" \
  LDFLAGS="$LDFLAGS"
fi


echo Exporting libint library
make export -j ${PARALLEL}

# turn into an xz archive instead
gunzip ${EXPORT_name}.tgz
xz -9 ${EXPORT_name}.tar

# copy to parent directory
cp ${EXPORT_name}.tar.xz ../../.
